package com.example.aprendiendo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import java.util.Random;

public class aprendernumeros extends AppCompatActivity {

    private TextView txtA, txtB, txtC, txtD, txtE, txtF, txtG, txtH, txtI, txtJ, txtK, txtL, txtM, txtN, txtÑ, txtO, txtP,
            txtQ, txtR, txtS, txtT, txtU, txtV, txtW, txtX, txtY, txtZ, txtAPOS, txtNumbers;
    private EditText txt1, txt2, txt3, txt4, txt5, txt6, txt7;
    private ImageView imgAhorcado;

    private Button txtJugar, txtRegresar;

    private int [] numeros = new int[7];
    private int numLetra, numPal = 0, con = 3, conBuenas = 0, conpul = 0;
    private String letra = "";
    private int num1, num2, errores = 0;

    MediaPlayer mp,mpull, mgood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aprendernumeros);

        this.txtA = (TextView) findViewById(R.id.txtA);
        this.txtB = (TextView) findViewById(R.id.txtB);
        this.txtC = (TextView) findViewById(R.id.txtC);
        this.txtD = (TextView) findViewById(R.id.txtD);
        this.txtE = (TextView) findViewById(R.id.txtE);
        this.txtF = (TextView) findViewById(R.id.txtF);
        this.txtG = (TextView) findViewById(R.id.txtG);
        this.txtH = (TextView) findViewById(R.id.txtH);
        this.txtI = (TextView) findViewById(R.id.txtI);
        this.txtJ = (TextView) findViewById(R.id.txtJ);
        this.txtK = (TextView) findViewById(R.id.txtK);
        this.txtL = (TextView) findViewById(R.id.txtL);
        this.txtM = (TextView) findViewById(R.id.txtM);
        this.txtN = (TextView) findViewById(R.id.txtN);
        this.txtÑ = (TextView) findViewById(R.id.txtÑ);
        this.txtO = (TextView) findViewById(R.id.txtO);
        this.txtP = (TextView) findViewById(R.id.txtP);
        this.txtQ = (TextView) findViewById(R.id.txtQ);
        this.txtR = (TextView) findViewById(R.id.txtR);
        this.txtS = (TextView) findViewById(R.id.txtS);
        this.txtT = (TextView) findViewById(R.id.txtT);
        this.txtU = (TextView) findViewById(R.id.txtU);
        this.txtV = (TextView) findViewById(R.id.txtV);
        this.txtW = (TextView) findViewById(R.id.txtW);
        this.txtX = (TextView) findViewById(R.id.txtX);
        this.txtY = (TextView) findViewById(R.id.txtY);
        this.txtZ = (TextView) findViewById(R.id.txtZ);
        this.txtAPOS = (TextView) findViewById(R.id.txtAPOS);

        this.txt1 = (EditText) findViewById(R.id.txt1);
        this.txt2 = (EditText) findViewById(R.id.txt2);
        this.txt3 = (EditText) findViewById(R.id.txt3);
        this.txt4 = (EditText) findViewById(R.id.txt4);
        this.txt5 = (EditText) findViewById(R.id.txt5);
        this.txt6 = (EditText) findViewById(R.id.txt6);
        this.txt7 = (EditText) findViewById(R.id.txt7);


        this.txtRegresar = (Button) findViewById(R.id.txtREGRESAR);
        this.txtJugar = (Button) findViewById(R.id.txtJUGAR);
        this.txtNumbers = (TextView)findViewById(R.id.txtNumbers);

        this.imgAhorcado = (ImageView)findViewById(R.id.imgAhorcado);
        txtNumbers.setText("");

        txtJugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt1.setText("");
                txt2.setText("");
                txt3.setText("");
                txt4.setText("");
                txt5.setText("");
                txt6.setText("");
                txt7.setText("");
                imgAhorcado.setImageResource(R.drawable.itemfaseuno);
                errores = 0;
                AsignarNumerosLetras();

                Random r = new Random();
                int na = r.nextInt(10) + 1;

                numPal = na;

                switch (numPal){
                    case 1:
                        num1 = r.nextInt(4) +1;
                        num2 = num1;
                        switch (num2){
                            case 1:
                                txt2.setText("T");
                                break;
                            case 2:
                                txt3.setText("O");
                                break;
                            case 3:
                                txt4.setText("B");
                                break;
                            case 4:
                                txt5.setText("I");
                                break;
                        }
                        break;
                    case 2:
                        num1 = r.nextInt(5) +1;
                        num2 = num1;
                        switch (num2){
                            case 1:
                                txt2.setText("C");
                                break;
                            case 2:
                                txt3.setText("H");
                                break;
                            case 3:
                                txt4.setText("U");
                                break;
                            case 4:
                                txt5.setText("P");
                                break;
                            case 5:
                                txt6.setText("A");
                                break;
                        }
                        break;
                    case 3:
                        num1 = r.nextInt(6) +1;
                        num2 = num1;
                        switch (num2){
                            case 1:
                                txt2.setText("C");
                                break;
                            case 2:
                                txt3.setText("H");
                                break;
                            case 3:
                                txt4.setText("O");
                                break;
                            case 4:
                                txt5.setText("N");
                                break;
                            case 5:
                                txt6.setText("N");
                                break;
                            case 6:
                                txt7.setText("A");
                                break;
                        }
                        break;
                    case 4:
                        num1 = r.nextInt(4) +1;
                        num2 = num1;
                        switch (num2){
                            case 1:
                                txt2.setText("T");
                                break;
                            case 2:
                                txt3.setText("A");
                                break;
                            case 3:
                                txt4.setText("P");
                                break;
                            case 4:
                                txt5.setText("A");
                                break;
                        }
                        break;
                    case 5:
                        num1 = r.nextInt(6) +1;
                        num2 = num1;
                        switch (num2){
                            case 1:
                                txt2.setText("G");
                                break;
                            case 2:
                                txt3.setText("A");
                                break;
                            case 3:
                                txt4.setText("A");
                                break;
                            case 4:
                                txt5.setText("Y");
                                break;
                            case 5:
                                txt6.setText("U");
                                break;
                            case 6:
                                txt7.setText("'");
                                break;
                        }
                        break;
                    case 6:
                        num1 = r.nextInt(7) +1;
                        num2 = num1;
                        switch (num2){
                            case 1:
                                txt1.setText("X");
                                break;
                            case 2:
                                txt2.setText("H");
                                break;
                            case 3:
                                txt3.setText("O");
                                break;
                            case 4:
                                txt4.setText("O");
                                break;
                            case 5:
                                txt5.setText("P");
                                break;
                            case 6:
                                txt6.setText("A");
                                break;
                            case 7:
                                txt7.setText("'");
                                break;
                        }
                        break;
                    case 7:
                        num1 = r.nextInt(5) +1;
                        num2 = num1;
                        switch (num2){
                            case 1:
                                txt2.setText("G");
                                break;
                            case 2:
                                txt3.setText("A");
                                break;
                            case 3:
                                txt4.setText("D");
                                break;
                            case 4:
                                txt5.setText("X");
                                break;
                            case 5:
                                txt6.setText("E");
                                break;
                        }
                        break;
                    case 8:
                        num1 = r.nextInt(5) +1;
                        num2 = num1;
                        switch (num2){
                            case 1:
                                txt2.setText("X");
                                break;
                            case 2:
                                txt3.setText("H");
                                break;
                            case 3:
                                txt4.setText("O");
                                break;
                            case 4:
                                txt5.setText("N");
                                break;
                            case 5:
                                txt6.setText("O");
                                break;
                        }
                        break;
                    case 9:
                        num1 = r.nextInt(3) +1;
                        num2 = num1;
                        switch (num2){
                            case 1:
                                txt3.setText("G");
                                break;
                            case 2:
                                txt4.setText("A");
                                break;
                            case 3:
                                txt5.setText("'");
                                break;
                        }
                        break;
                    case 10:
                        num1 = r.nextInt(4) +1;
                        num2 = num1;
                        switch (num2){
                            case 1:
                                txt2.setText("C");
                                break;
                            case 2:
                                txt3.setText("H");
                                break;
                            case 3:
                                txt4.setText("I");
                                break;
                            case 4:
                                txt5.setText("I");
                                break;
                        }
                        break;
                }

                if (numPal == 10){
                    float tamtxt = 125;
                    txtNumbers.setTextSize(tamtxt);
                    txtNumbers.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                    txtNumbers.setText("" + numPal);
                }else{
                    float ttxt = 180;
                    txtNumbers.setTextSize(ttxt);
                    txtNumbers.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    txtNumbers.setText("" + numPal);
                }
                System.out.println("el numero aleatorio es: " + numPal);
                cantidadLetras();
                mpull.start();
            }
        });

        txtRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(aprendernumeros.this, Home.class);
                startActivity(intent);
            }
        });

    }

    private void AsignarNumerosLetras(){

        mpull = MediaPlayer.create(this, R.raw.pulsacion);

        this.txtA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "A";
                numLetra = 1;
                escribir();
            }
        });
        this.txtB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "B";
                numLetra = 2;
                escribir();
            }
        });
        this.txtC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "C";
                numLetra = 3;
                escribir();
            }
        });
        this.txtD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "D";
                numLetra = 4;
                escribir();
            }
        });
        this.txtE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "E";
                numLetra = 5;
                escribir();
            }
        });
        this.txtF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "F";
                numLetra = 6;
                escribir();
            }
        });
        this.txtG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "G";
                numLetra = 7;
                escribir();
            }
        });
        this.txtH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "H";
                numLetra = 8;
                escribir();
            }
        });
        this.txtI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "I";
                numLetra = 9;
                escribir();
            }
        });
        this.txtJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "J";
                numLetra = 10;
                escribir();
            }
        });
        this.txtK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "K";
                numLetra = 11;
                escribir();
            }
        });
        this.txtL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "L";
                numLetra = 12;
                escribir();
            }
        });
        this.txtM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "M";
                numLetra = 13;
                escribir();
            }
        });
        this.txtN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "N";
                numLetra = 14;
                escribir();
            }
        });
        this.txtÑ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "Ñ";
                numLetra = 15;
                escribir();
            }
        });
        this.txtO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "O";
                numLetra = 16;
                escribir();
            }
        });
        this.txtP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "P";
                numLetra = 17;
                escribir();
            }
        });
        this.txtQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "Q";
                numLetra = 18;
                escribir();
            }
        });
        this.txtR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "R";
                numLetra = 19;
                escribir();
            }
        });
        this.txtS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "S";
                numLetra = 20;
                escribir();
            }
        });
        this.txtT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "T";
                numLetra = 21;
                escribir();
            }
        });
        this.txtU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "U";
                numLetra = 22;
                escribir();
            }
        });
        this.txtV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "V";
                numLetra = 23;
                escribir();
            }
        });
        this.txtW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "W";
                numLetra = 24;
                escribir();
            }
        });
        this.txtX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "X";
                numLetra = 25;
                escribir();
            }
        });
        this.txtY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "Y";
                numLetra = 26;
                escribir();
            }
        });
        this.txtZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "Z";
                numLetra = 27;
                escribir();
            }
        });
        this.txtAPOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "'";
                numLetra = 28;
                escribir();
            }
        });
    }

    private void QuitarNumerosLetra(){
        this.txtA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "A";
            }
        });
        this.txtB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "B";
            }
        });
        this.txtC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "C";
            }
        });
        this.txtD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "D";
            }
        });
        this.txtE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "E";
            }
        });
        this.txtF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "F";
            }
        });
        this.txtG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "G";
            }
        });
        this.txtH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "H";
            }
        });
        this.txtI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "I";
            }
        });
        this.txtJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "J";
            }
        });
        this.txtK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "K";
            }
        });
        this.txtL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "L";
            }
        });
        this.txtM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "M";
            }
        });
        this.txtN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "N";
            }
        });
        this.txtÑ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "Ñ";
            }
        });
        this.txtO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "O";
            }
        });
        this.txtP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "P";
            }
        });
        this.txtQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "Q";
            }
        });
        this.txtR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "R";
            }
        });
        this.txtS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "S";
            }
        });
        this.txtT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "T";
            }
        });
        this.txtU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "U";
            }
        });
        this.txtV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "V";
            }
        });
        this.txtW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "W";
            }
        });
        this.txtX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "X";
            }
        });
        this.txtY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "Y";
            }
        });
        this.txtZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "Z";
            }
        });
        this.txtAPOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                letra = "'";
            }
        });
    }

    private void escribir() {

        switch (numPal){
            case 1:
                switch (numLetra){
                    case 21:
                        txt2.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 4){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 16:
                        txt3.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 4){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 2:
                        txt4.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 4){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 9:
                        txt5.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 4){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        checharError();
                        break;
                }
                break;
            case 2:
                switch (numLetra){
                    case 3:
                        txt2.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 8:
                        txt3.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 22:
                        txt4.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 17:
                        txt5.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        txt6.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        checharError();
                        break;
                }
                break;
            case 3:
                switch (numLetra){
                    case 3:
                        txt2.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 8:
                        txt3.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 16:
                        txt4.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 14:
                        txt5.setText(letra);
                        txt6.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        txt7.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        checharError();
                        break;
                }
                break;
            case 4:
                switch (numLetra){
                    case 21:
                        txt2.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 3){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        txt3.setText(letra);
                        txt5.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 3){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 17:
                        txt4.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 3){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        checharError();
                        break;
                }
                break;
            case 5:
                switch (numLetra){
                    case 7:
                        txt2.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        txt3.setText(letra);
                        txt4.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 26:
                        txt5.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 22:
                        txt6.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 28:
                        txt7.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        checharError();
                        break;
                }
                break;
            case 6:
                switch (numLetra){
                    case 25:
                        txt1.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 6){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 8:
                        txt2.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 6){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 16:
                        txt3.setText(letra);
                        txt4.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 6){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 17:
                        txt5.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 6){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        txt6.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 6){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 28:
                        txt7.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 6){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        checharError();
                        break;
                }
                break;
            case 7:
                switch (numLetra){
                    case 7:
                        txt2.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        txt3.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 4:
                        txt4.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 25:
                        txt5.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 5:
                        txt6.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 5){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        checharError();
                        break;
                }
                break;
            case 8:
                switch (numLetra){
                    case 25:
                        txt2.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 4){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 8:
                        txt3.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 4){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 16:
                        txt4.setText(letra);
                        txt6.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 4){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 14:
                        txt5.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 4){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        checharError();
                        break;
                }
                break;
            case 9:
                switch (numLetra){
                    case 7:
                        txt3.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 3){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 1:
                        txt4.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 3){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 28:
                        txt5.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 3){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        checharError();
                        break;
                }
                break;
            case 10:
                switch (numLetra){
                    case 3:
                        txt2.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 3){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 8:
                        txt3.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 3){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 9:
                        txt4.setText(letra);
                        txt5.setText(letra);
                        mpull.start();
                        conBuenas = conBuenas +1;
                        if (conBuenas == 3){
                            mgood = MediaPlayer.create(this, R.raw.ok);
                            mgood.start();
                            conBuenas = 0;
                            Toast.makeText(this, "Felicidades escribiste la palabra correctamente", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        checharError();
                        break;
                }
                break;
        }

    }

    private void checharError() {
        mp = MediaPlayer.create(this, R.raw.error);
        imgAhorcado.setImageResource(R.drawable.itemfaseuno);
        mp.start();
        Toast.makeText(this, "Intenta con otra letra", Toast.LENGTH_SHORT).show();
        errores++;
            if (errores == 1){
                imgAhorcado.setImageResource(R.drawable.itemfasedos);
                mp.start();
                Toast.makeText(this, "Intenta con otra letra", Toast.LENGTH_SHORT).show();
            }else {
                if (errores == 2) {
                    imgAhorcado.setImageResource(R.drawable.itemfasetres);
                    mp.start();
                    Toast.makeText(this, "Intenta con otra letra", Toast.LENGTH_SHORT).show();
                }else {
                    if (errores == 3) {
                        imgAhorcado.setImageResource(R.drawable.itemfasecuatro);
                        mp.start();
                        Toast.makeText(this, "Intenta con otra letra", Toast.LENGTH_SHORT).show();
                    }else {
                        if (errores == 4) {
                            imgAhorcado.setImageResource(R.drawable.itemfasecinco);
                            mp.start();
                            Toast.makeText(this, "Intenta con otra letra", Toast.LENGTH_SHORT).show();
                        }else {
                            if (errores == 5) {
                                imgAhorcado.setImageResource(R.drawable.itemfaseseis);
                                mp.start();
                                Toast.makeText(this, "Intenta con otra letra", Toast.LENGTH_SHORT).show();
                            }else {
                                if (errores == 6) {
                                    imgAhorcado.setImageResource(R.drawable.itemfasesiete);
                                    mp.start();
                                    errores = 0;
                                    Toast.makeText(this, "Se te acabaron las oportunidades", Toast.LENGTH_SHORT).show();
                                    QuitarNumerosLetra();
                                }
                            }
                        }
                    }
                }
            }
        }

    private void cantidadLetras(){
        switch (numPal){
            case 1:
                txt1.setVisibility(View.INVISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.INVISIBLE);
                txt7.setVisibility(View.INVISIBLE);
                //Toast.makeText(this, "El numero es 1", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                txt1.setVisibility(View.INVISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.INVISIBLE);
                //Toast.makeText(this, "El numero es 2", Toast.LENGTH_SHORT).show();
                break;
            case 3:
                txt1.setVisibility(View.INVISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.VISIBLE);
                //Toast.makeText(this, "El numero es 3", Toast.LENGTH_SHORT).show();
                break;
            case 4:
                txt1.setVisibility(View.INVISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.INVISIBLE);
                txt7.setVisibility(View.INVISIBLE);
                //Toast.makeText(this, "El numero es 4", Toast.LENGTH_SHORT).show();
                break;
            case 5:
                txt1.setVisibility(View.INVISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.VISIBLE);
                //Toast.makeText(this, "El numero es 5", Toast.LENGTH_SHORT).show();
                break;
            case 6:
                txt1.setVisibility(View.VISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.VISIBLE);
                //Toast.makeText(this, "El numero es 6", Toast.LENGTH_SHORT).show();
                break;
            case 7:
                txt1.setVisibility(View.INVISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.INVISIBLE);
                //Toast.makeText(this, "El numero es 7", Toast.LENGTH_SHORT).show();
                break;
            case 8:
                txt1.setVisibility(View.INVISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.VISIBLE);
                txt7.setVisibility(View.INVISIBLE);
                //Toast.makeText(this, "El numero es 8", Toast.LENGTH_SHORT).show();
                break;
            case 9:
                txt1.setVisibility(View.INVISIBLE);
                txt2.setVisibility(View.INVISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.INVISIBLE);
                txt7.setVisibility(View.INVISIBLE);
                //Toast.makeText(this, "El numero es 9", Toast.LENGTH_SHORT).show();
                break;
            case 10:
                txt1.setVisibility(View.INVISIBLE);
                txt2.setVisibility(View.VISIBLE);
                txt3.setVisibility(View.VISIBLE);
                txt4.setVisibility(View.VISIBLE);
                txt5.setVisibility(View.VISIBLE);
                txt6.setVisibility(View.INVISIBLE);
                txt7.setVisibility(View.INVISIBLE);
                //Toast.makeText(this, "El numero es 10", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void generar(){
        int con = 0;

        while (con < 7){
            Random r = new Random();
            int nAleatorio = r.nextInt(7) + 1;
            int nvr = 0;

            for (int i = 0; i < 7; i++){
                if (numeros[i] == nAleatorio){
                    nvr++;
                }
            }
            if (nvr < 1){
                numeros[con] = nAleatorio;
                con++;
            }
        }
    }
}
