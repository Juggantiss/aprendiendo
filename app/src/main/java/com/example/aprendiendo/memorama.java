package com.example.aprendiendo;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;


public class memorama extends AppCompatActivity {
    private static final int[] CARTA_RESOURCE= new int[]{
            R.drawable.aguacate,
            R.drawable.tomate,
            R.drawable.sandia,
            R.drawable.bananas,
            R.drawable.calabaza,
            R.drawable.mangos,

    };
private  final Handler handler = new Handler();
private Carta[] cartas;
private boolean touchActivo=true;
private Carta visible=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final TableLayout tabla= new TableLayout(this);
        final int tam=4;
        cartas=(crearCeldas(tam*tam));
        Collections.shuffle(Arrays.asList(cartas));
        for(int y=0; y< tam;y++){
            final TableRow fila = new TableRow(this);
            for(int x=0; x< tam; x++){
                fila.addView(cartas[(y*tam)+x].boton);

            }
                tabla.addView(fila);

        }
        setContentView(tabla);

    }
    private class Carta implements View.OnClickListener{
        private final ImageButton boton;
        private final int imagen;
        private boolean  caraVisible=false;
        Carta(final int imagen){
        this.imagen=imagen;
        this.boton=new ImageButton(memorama.this);
        this.boton.setLayoutParams(new TableRow.LayoutParams(64, 64));
        this.boton.setScaleType(ImageView.ScaleType.FIT_XY);
        this.boton.setImageResource(R.drawable.linea);
        this.boton.setOnClickListener(this);
        }
        void setCaraVisible (final boolean caraVisible){
            this.caraVisible = caraVisible;
            boton.setImageResource(caraVisible? imagen : R.drawable.linea);
        }

        public void onClick(View arg0){
        if(!caraVisible&&touchActivo){
            onCartaDescubierta(this);

        }

        }



    }
    private Carta[] crearCeldas(final int  cont){
        final Carta[]  array= new Carta[cont];
        for(int i=0;i < array.length;i++){
        array[i]= new Carta(CARTA_RESOURCE[i/2]);

        }
        return array;
    }
    public  void onCartaDescubierta(final Carta celda){
        if(visible==null){
            visible=celda;
            visible.setCaraVisible(true);
        }else if(visible.imagen==celda.imagen){
            celda.setCaraVisible(true);
            celda.boton.setEnabled(false);
            visible.boton.setEnabled(false);
            visible=null;
        }else{
            celda.setCaraVisible(true);
            touchActivo=false;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    celda.setCaraVisible(false);
                    visible.setCaraVisible(false);
                    visible=null;
                    touchActivo=true;
                }
            },1000);
        }
    }
}


