package com.example.aprendiendo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class aprendeanimales extends AppCompatActivity implements View.OnClickListener{

    private android.widget.TextView txtNameAnimal;
    private android.widget.TextView txtOption1Animal1;
    private android.support.v7.widget.CardView Option1Animal1;
    private android.widget.TextView txtOption1Animal2;
    private android.support.v7.widget.CardView Option1Animal2;
    private android.widget.TextView txtOption1Animal3;
    private android.support.v7.widget.CardView Option1Animal3;
    private android.widget.TextView txtOption1Animal4;
    private android.support.v7.widget.CardView Option1Animal4;
    private android.widget.Button btncalificar;

    private String op1, op2, op3, op4;
    private int op_select, aciertos = 0, nivels = 1;
    private Button btnSiguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aprendeanimales);
        this.btnSiguiente = (Button) findViewById(R.id.btnSiguiente);
        this.btncalificar = (Button) findViewById(R.id.btncalificar);
        this.Option1Animal4 = (CardView) findViewById(R.id.Option1Animal4);
        this.txtOption1Animal4 = (TextView) findViewById(R.id.txtOption1Animal4);
        this.Option1Animal3 = (CardView) findViewById(R.id.Option1Animal3);
        this.txtOption1Animal3 = (TextView) findViewById(R.id.txtOption1Animal3);
        this.Option1Animal2 = (CardView) findViewById(R.id.Option1Animal2);
        this.txtOption1Animal2 = (TextView) findViewById(R.id.txtOption1Animal2);
        this.Option1Animal1 = (CardView) findViewById(R.id.Option1Animal1);
        this.txtOption1Animal1 = (TextView) findViewById(R.id.txtOption1Animal1);
        this.txtNameAnimal = (TextView) findViewById(R.id.txtNameAnimal);

        Option1Animal1.setOnClickListener(this);
        Option1Animal2.setOnClickListener(this);
        Option1Animal3.setOnClickListener(this);
        Option1Animal4.setOnClickListener(this);

        btncalificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                metodoCalificar(op_select);
            }
        });

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                niveles(nivels);
                btncalificar.setEnabled(true);
                btnSiguiente.setEnabled(false);
                Option1Animal1.setCardBackgroundColor(0xFFededed);
                Option1Animal2.setCardBackgroundColor(0xFFededed);
                Option1Animal3.setCardBackgroundColor(0xFFededed);
                Option1Animal4.setCardBackgroundColor(0xFFededed);
            }
        });

    }

    private void niveles(int nivel){
        switch (nivel){
            //Este es el nivel 1
            case 1:
                txtNameAnimal.setText("Tortuga");
                txtOption1Animal1.setText("bigu");
                txtOption1Animal2.setText("bigose");
                txtOption1Animal3.setText("benda");
                txtOption1Animal4.setText("meuxubi");
                //metodoCalificar(op_select);
                break;
            //Este es el nivel 2
            case 2:
                txtNameAnimal.setText("Marrano");
                txtOption1Animal1.setText("bihui");
                txtOption1Animal2.setText("ngupi");
                txtOption1Animal3.setText("migu");
                txtOption1Animal4.setText("bizu");
                //metodoCalificar(op_select);
                break;
        }
    }

    private void metodoCalificar(int opcion){

        if (nivels == 1){
            switch (opcion){
                case 1:
                    Option1Animal1.setCardBackgroundColor(0xFF43a047);
                    btncalificar.setEnabled(false);
                    btnSiguiente.setEnabled(true);
                    aciertos = aciertos + 1;
                    nivels = 2;
                    break;
                case 2:
                    Option1Animal2.setCardBackgroundColor(0xFFd50000);
                    Option1Animal1.setCardBackgroundColor(0xFF43a047);
                    btncalificar.setEnabled(false);
                    btnSiguiente.setEnabled(true);
                    aciertos = 0;
                    nivels = 2;
                    break;
                case 3:
                    Option1Animal3.setCardBackgroundColor(0xFFd50000);
                    Option1Animal1.setCardBackgroundColor(0xFF43a047);
                    btncalificar.setEnabled(false);
                    btnSiguiente.setEnabled(true);
                    aciertos = 0;
                    nivels = 2;
                    break;
                case 4:
                    Option1Animal4.setCardBackgroundColor(0xFFd50000);
                    Option1Animal1.setCardBackgroundColor(0xFF43a047);
                    btncalificar.setEnabled(false);
                    btnSiguiente.setEnabled(true);
                    aciertos = 0;
                    nivels = 2;
                    break;
                default:
                    Toast.makeText(this, "Selecciona una opcion primero", Toast.LENGTH_SHORT).show();
                    break;
            }
        }else if (nivels == 2){
            switch (opcion){
                case 1:
                    Option1Animal1.setCardBackgroundColor(0xFF43a047);
                    btncalificar.setEnabled(false);
                    btnSiguiente.setEnabled(true);
                    aciertos = aciertos + 1;
                    nivels = 2;
                    Toast.makeText(this, "Tus aciertos son: " + aciertos, Toast.LENGTH_SHORT).show();
                    btnSiguiente.setEnabled(false);
                    break;
                case 2:
                    Option1Animal2.setCardBackgroundColor(0xFFd50000);
                    Option1Animal1.setCardBackgroundColor(0xFF43a047);
                    btncalificar.setEnabled(false);
                    btnSiguiente.setEnabled(true);
                    //aciertos = 0;
                    nivels = 2;
                    Toast.makeText(this, "Tus aciertos son: " + aciertos, Toast.LENGTH_SHORT).show();
                    btnSiguiente.setEnabled(false);
                    break;
                case 3:
                    Option1Animal3.setCardBackgroundColor(0xFFd50000);
                    Option1Animal1.setCardBackgroundColor(0xFF43a047);
                    btncalificar.setEnabled(false);
                    btnSiguiente.setEnabled(true);
                    //aciertos = 0;
                    nivels = 2;
                    Toast.makeText(this, "Tus aciertos son: " + aciertos, Toast.LENGTH_SHORT).show();
                    btnSiguiente.setEnabled(false);
                    break;
                case 4:
                    Option1Animal4.setCardBackgroundColor(0xFFd50000);
                    Option1Animal1.setCardBackgroundColor(0xFF43a047);
                    btncalificar.setEnabled(false);
                    btnSiguiente.setEnabled(true);
                    //aciertos = 0;
                    nivels = 2;
                    Toast.makeText(this, "Tus aciertos son: " + aciertos, Toast.LENGTH_SHORT).show();
                    btnSiguiente.setEnabled(false);
                    break;
                default:
                    Toast.makeText(this, "Selecciona una opcion primero", Toast.LENGTH_SHORT).show();
                    break;
            }
        }

    }

    private void calificar(int op_selec) {

        switch (op_selec){
            case 1:
                Option1Animal1.setCardBackgroundColor(0xFF43a047);
                btncalificar.setEnabled(false);
                btnSiguiente.setEnabled(true);
                aciertos = aciertos + 1;
                break;
            case 2:
                Option1Animal2.setCardBackgroundColor(0xFFd50000);
                Option1Animal1.setCardBackgroundColor(0xFF43a047);
                btncalificar.setEnabled(false);
                btnSiguiente.setEnabled(true);
                aciertos = 0;
                break;
            case 3:
                Option1Animal3.setCardBackgroundColor(0xFFd50000);
                Option1Animal1.setCardBackgroundColor(0xFF43a047);
                btncalificar.setEnabled(false);
                btnSiguiente.setEnabled(true);
                aciertos = 0;
                break;
            case 4:
                Option1Animal4.setCardBackgroundColor(0xFFd50000);
                Option1Animal1.setCardBackgroundColor(0xFF43a047);
                btncalificar.setEnabled(false);
                btnSiguiente.setEnabled(true);
                aciertos = 0;
                break;
            default:
                Toast.makeText(this, "Selecciona una opcion primero", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.Option1Animal1 :
                op_select = 1;
                Option1Animal1.setCardBackgroundColor(0xFFffeb3b);
                Option1Animal2.setCardBackgroundColor(0xFFededed);
                Option1Animal3.setCardBackgroundColor(0xFFededed);
                Option1Animal4.setCardBackgroundColor(0xFFededed);
                break;
            case R.id.Option1Animal2 :
                op_select = 2;
                Option1Animal1.setCardBackgroundColor(0xFFededed);
                Option1Animal2.setCardBackgroundColor(0xFFffeb3b);
                Option1Animal3.setCardBackgroundColor(0xFFededed);
                Option1Animal4.setCardBackgroundColor(0xFFededed);
                break;
            case R.id.Option1Animal3 :
                op_select = 3;
                Option1Animal1.setCardBackgroundColor(0xFFededed);
                Option1Animal2.setCardBackgroundColor(0xFFededed);
                Option1Animal3.setCardBackgroundColor(0xFFffeb3b);
                Option1Animal4.setCardBackgroundColor(0xFFededed);
                break;
            case R.id.Option1Animal4 :
                op_select = 4;
                Option1Animal1.setCardBackgroundColor(0xFFededed);
                Option1Animal2.setCardBackgroundColor(0xFFededed);
                Option1Animal3.setCardBackgroundColor(0xFFededed);
                Option1Animal4.setCardBackgroundColor(0xFFffeb3b);
                break;
            default:break;
        }
    }
}
