package com.example.aprendiendo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;

public class Home extends AppCompatActivity implements View.OnClickListener {

    private android.support.v7.widget.CardView infozapcardId;
    private android.support.v7.widget.CardView frutscardId;
    private android.support.v7.widget.CardView numberscardId;
    private android.support.v7.widget.CardView palabrascardId;
    private android.support.v7.widget.CardView aboutcardId;
    private CardView animalcardId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        this.animalcardId = (CardView) findViewById(R.id.animalcardId);
        this.aboutcardId = (CardView) findViewById(R.id.aboutcardId);
        this.palabrascardId = (CardView) findViewById(R.id.palabrascardId);
        this.numberscardId = (CardView) findViewById(R.id.numberscardId);
        this.frutscardId = (CardView) findViewById(R.id.frutscardId);
        this.infozapcardId = (CardView) findViewById(R.id.infozapcardId);

        //Añadir clicklistener a las cartas
        aboutcardId.setOnClickListener(this);
        palabrascardId.setOnClickListener(this);
        numberscardId.setOnClickListener(this);
        frutscardId.setOnClickListener(this);
        infozapcardId.setOnClickListener(this);
        animalcardId.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent i;

        switch (v.getId()){
            case R.id.aboutcardId : i = new Intent(this, acercade.class); startActivity(i); break;
            case R.id.infozapcardId : i = new Intent(this, Informacionactual.class); startActivity(i); break;
            case R.id.frutscardId : i = new Intent(this, aprenderfruta.class); startActivity(i); break;
            case R.id.numberscardId : i = new Intent(this, aprendernumeros.class); startActivity(i); break;
            case R.id.palabrascardId : i = new Intent(this, hablanteszapoteco.class); startActivity(i); break;
            case R.id.animalcardId : i = new Intent(this, aprendeanimales.class); startActivity(i); break;
            default:break;
        }

    }
}
