package com.example.aprendiendo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class slide extends PagerAdapter  {

    Context context;
    LayoutInflater LayoutInflater;

    public slide(Context context){
        this.context=context;
    }

    public int[] slide_images ={
            R.drawable.ione,
            R.drawable.ione,
            R.drawable.mapai


    };
    public String[] slide_headings = {
            "ZAPOTECO",
            "ALFABETO",
            "ACTUALIDAD",


    };
    public String[] slide_desc ={

            "El zapoteco del Istmo se habla en una área amplia del Istmo de Tehuantepec en el estado de Oaxaca, México.\n" +
                    "        El área consta de dos distritos (Juchitán y Tehuantepec) y quince municipios.",
            "El alfabeto que empleamos […] para la escritura del zapoteco del Istmo es el aprobado en las sesiones de Mesa Redonda celebradas en la Ciudad de México, en 1956:\n" +
                    " a - b - c - ch - d - dx - e - f - g - h - i - j - l - m - n - ñ - o - p - q - r - rr - s - t - u - x - xh - y - z. " ,
           "Hoy en día los zapotecas se dividen en dos grupos. El más grande se localiza al sur \n" +
            "de la sierra de Oaxaca y el otro al sur del istmo de Tehuantepec, y la los indigenas se siente orgullosos de el. ",

    };


    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater=(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView slideImageView = (ImageView) view.findViewById(R.id.slideimage);
        TextView slideHeading = (TextView)  view.findViewById(R.id.arriba1);
        TextView SlideDescripcion = (TextView) view.findViewById(R.id.abajo1);


        slideImageView.setImageResource(slide_images[position]);
        slideHeading.setText(slide_headings[position]);
        SlideDescripcion.setText(slide_desc[position]);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout)object);


    }
}