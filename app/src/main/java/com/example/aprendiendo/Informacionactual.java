package com.example.aprendiendo;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Informacionactual extends AppCompatActivity {


    private ViewPager mSlideViewPager;
    private LinearLayout mDatos;
    private TextView[] Dots;
    private slide sliderAdapter;

    private Button next;
    private Button back;
    private int CurrentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacionactual);

        mSlideViewPager=(ViewPager) findViewById(R.id.slideViewPager);
        mDatos= (LinearLayout) findViewById(R.id.dato);

        next=(Button) findViewById(R.id.next);
        back=(Button) findViewById(R.id.prev);

        sliderAdapter = new slide(this);
        mSlideViewPager.setAdapter(sliderAdapter);

        doctsIndicator(0);
        mSlideViewPager.addOnPageChangeListener(viewListener);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSlideViewPager.setCurrentItem(CurrentPage + 1);

            }
        });
      back.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              mSlideViewPager.setCurrentItem(CurrentPage - 1);
          }
      });
    }

    public void doctsIndicator(int position){
        mDatos.removeAllViews();
        Dots= new TextView [3];

        for(int i=0;i<Dots.length;i++){
            Dots[i] = new TextView (this);
            Dots[i].setText(Html.fromHtml("&#8226;"));
            Dots[i].setTextColor(getResources().getColor(R.color.colorPrimary));
            Dots[i].setTextSize(35);

            mDatos.addView(Dots[i]);

        }

        if(Dots.length > 0){
            Dots[position].setTextColor(getResources().getColor(R.color.black_overlay));

        }


    }
    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            doctsIndicator(i);
            CurrentPage = i;

            if(i==0){
                next.setEnabled(true);
                back.setEnabled(false);
                back.setVisibility(View.INVISIBLE);

                next.setText("Siguiente");
                back.setText("");

            } else if(i== Dots.length -1){
                next.setEnabled(true);
                back.setEnabled(true);
                back.setVisibility(View.VISIBLE);

                next.setText("Final");
                back.setText("Regresar");
            }else{
                next.setEnabled(true);
                back.setEnabled(true);
                back.setVisibility(View.VISIBLE);

                next.setText("Siguiente");
                back.setText("Regresar");
            }
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };
}
